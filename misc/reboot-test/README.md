# reboot test
Test Maintainer: [Bruno Goncalves](mailto:bgoncalv@redhat.com)

## How to run it


### Install dependencies
```bash
root# bash ../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
